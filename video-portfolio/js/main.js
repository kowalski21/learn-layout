const getNav = document.getElementById("nav");
const getNavBtn = document.getElementById("nav-btn");
const getNavBtnImg = document.getElementById("nav-btn-img");

getNavBtn.addEventListener('click', () => {
  if (getNav.classList.toggle('open')) {
    getNavBtnImg.src = "images/icons/nav-close.svg"
  } else {
    getNavBtnImg.src = "images/icons/nav-open.svg"
  }
})

AOS.init({
  // disable: "mobile",
  // once: true
});

